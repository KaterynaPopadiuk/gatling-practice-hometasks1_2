package htGatling

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.language.postfixOps

class RecordedSimulation extends Simulation {

	val httpProtocol = http
		.baseUrl("https://challenge.flood.io")
		.inferHtmlResources(BlackList(""".*\.js""",""".*\.css""",""".*css.*""",""".*\.gif""",""".*\.jpeg""",""".*\.jpg""",""".*\.ico""",""".*\.png"""), WhiteList())
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("uk-UA,uk;q=0.8,en-US;q=0.5,en;q=0.3")
		.userAgentHeader("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:97.0) Gecko/20100101 Firefox/97.0")
		.disableFollowRedirect

	val headers_0 = Map(
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "none",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map(
		"Origin" -> "https://challenge.flood.io",
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "same-origin",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_5 = Map(
		"Accept" -> "*/*",
		"If-None-Match" -> """"cfa790f3316698e22babd05a8063165a"""",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_7 = Map(
		"Accept" -> "text/html, application/xhtml+xml",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		"Turbolinks-Referrer" -> "https://challenge.flood.io/done")



	val scn = scenario("RecordedSimulation")
		.exec(http("Open Home Page")
			.get("/")
			.headers(headers_0)
		.check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"").find.saveAs("authenticity_token"))
		.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))

		.pause(4)

		.exec(http("Click Start Button")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "1")
			.formParam("commit", "Start")
		.check(status.is(302)))
		.pause(5)
		.exec(http("Select Age")
			.get("/step/2")
			.headers(headers_0)
			.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))
		.exec(http("Click Next Button")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "2")
			.formParam("challenger[age]", "26")
			.formParam("commit", "Next")
		.check(status.is(302)))
		.pause(9)
		.exec(actionBuilder = http("Select Maximum Value")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "3")
			.check(css(".collection_radio_buttons").findAll.transform(list => list.map(_.toInt).max).saveAs("largest_order"))
			.check(regex(".*order_selected.*value=\"(.*?)\"").find.saveAs("order_selected"))
			.formParam("commit", "Next")
			.check(status.is(302)))
		.pause(5)
		.exec(http("Next Step")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "4")
			.formParam("challenger[order_5]", "1645296510")
			.formParam("challenger[order_8]", "1645296510")
			.formParam("challenger[order_5]", "1645296510")
			.formParam("challenger[order_11]", "1645296510")
			.formParam("challenger[order_9]", "1645296510")
			.formParam("challenger[order_9]", "1645296510")
			.formParam("challenger[order_14]", "1645296510")
			.formParam("challenger[order_13]", "1645296510")
			.formParam("challenger[order_8]", "1645296510")
			.formParam("challenger[order_11]", "1645296510")
			.formParam("commit", "Next")
			.resources(http("request_5")
			.get("/code")
			.headers(headers_5))
			.check(status.is(302)))
		.pause(19)
		.exec(http("Enter Number")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "5")
			.formParam("challenger[one_time_token]", "2777777788")
			.formParam("commit", "Next")
			.check(status.is(302)))
		.exec(http("Done")
			.get("/")
			.headers(headers_7))

	setUp(scn.inject(rampUsers(5)during(60 seconds)).protocols(httpProtocol))
}